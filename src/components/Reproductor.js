import React, { useState, useEffect } from 'react';
import { Audio } from 'expo-av';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import playButton from '../../assets/play.png'; 
import pauseButton from '../../assets/pause.png'; 

const soundObject = new Audio.Sound();

export default function Reproductor() {
    const [isPlaying, setIsPlaying] = useState(false);
    const [audioURL, setAudioURL] = useState('https://radio.lalibre.net/radiohumedales');
    
  
    useEffect(() => {
      Audio.setAudioModeAsync({
        staysActiveInBackground: true,
      });
      loadSound();
    }, []);

    const loadSound = async () => {
        try {
          await soundObject.loadAsync({ uri: audioURL });
        } catch (error) {
          // An error occurred!
        }
      }
  
      const playSound = async () => {
        try {
          await soundObject.playAsync();
          setIsPlaying(true);
        } catch (error) {
          // An error occurred!
        }
      }
    
      const pauseSound = async () => {
        try {
          await soundObject.pauseAsync();
          setIsPlaying(false);
        } catch (error) {
          // An error occurred!
        }
      }

    return (
        <View>
      <TouchableOpacity onPress={isPlaying ? pauseSound : playSound}>
        <Image style={styles.playPauseButton} source={isPlaying ? pauseButton : playButton} />
      </TouchableOpacity>
    </View>
    );
}


const styles = StyleSheet.create({
    playPauseButton: {
      width: 80, 
      height: 80, 
      resizeMode: 'contain',
      paddingTop: 40,
    },
  });