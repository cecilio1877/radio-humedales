import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { Image, Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import logo from './assets/logo.png'; // Asegúrate de que la ruta sea correcta
import facebook from './assets/facebook.png';
import twitter from './assets/twitter.png';
import instagram from './assets/instagram.png';
import whatsapp from './assets/whatsapp.png';
import { Linking } from 'react-native';
import Reproductor from './src/components/Reproductor';



export default function App() {
  return (
    <View style={styles.container}>
      <Image source={logo} style={styles.logo}></Image>
      <Text style={styles.texto}>Somos una radio libre, comunitaria, feminista, autónoma, 100% autogestionada y sin fines de lucro.

¡Estamos al aire desde julio de 2017, gracias al trabajo activista de nuestra colectiva radialista y también gracias a tu apoyo!</Text>
<View style={styles.box}>
        <Text style={styles.textoEscuchanos}>Escuchanos aqui!</Text>
        <Reproductor />
          
      </View>
      <View style={styles.redes}>
        <Text>Síguenos en:</Text>
        <View style={styles.socialMediaContainer}>
        <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/culturayexistencialesbica/')}>
          <Image source={facebook} style={styles.socialMediaIcon}></Image>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/radiohumedales')}>
          <Image source={twitter} style={styles.socialMediaIcon}></Image>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/radio_humedales/')}>
          <Image source={instagram} style={styles.socialMediaIcon}></Image>
        </TouchableOpacity>
      </View>
      
      
      </View>

      <View>
      <TouchableOpacity onPress={() => Linking.openURL('https://wa.link/9e5a7d')}>
          <Image source={whatsapp} style={styles.whatsApp}></Image>
        </TouchableOpacity>
      </View>
      
      <View>
      <TouchableOpacity onPress={() => Linking.openURL('https://lalibre.net/')}>
        <Text style={styles.footer}>Hecho con ❤ por <Text style={{textDecorationLine: 'underline'}}> LaLibre.net </Text></Text></TouchableOpacity>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#57C2B0',
    /* alignItems: 'center', */
    justifyContent: 'center',
  },
  
  logo: {
    width: 190, 
    height: 190, 
    resizeMode: 'contain', 
    alignSelf: 'center',
  },
  box: {
    backgroundColor: '#FAE48E',
    padding: 30, 
    alignItems: 'center',
    width: 300, 
    height: 180,
    borderColor: 'black',
    borderWidth: 3,
    borderRadius: 10, 
    alignSelf: 'center',

  },
  texto:
  {
    color: 'black',
    textAlign: 'center',
    fontSize: 15,
    padding: 20,
  }, 
  redes: {
    alignItems: 'center',
    width: 350,
    height: 110,
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    
    
  },
  socialMediaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: 180,
    paddingTop: 20,

  },
  socialMediaIcon: {
    width: 35,
    height: 35, 
    resizeMode: 'contain',
    
  },
  whatsApp: {
    width: 50, 
    height: 50, 
    resizeMode: 'contain',
    justifyContent: 'center',
    alignSelf: 'center',  
    
    
   
  },
  footer: {
    color: 'black',
    textAlign: 'center',
    fontSize: 11,
    padding: 20,
  },
  textoEscuchanos: {
    color: 'black',
    textAlign: 'center',
    fontSize: 18,
    padding: 10,
   
  },
});
