# Radio Humedales

Aplicación móvil creada con React Native

## Instalación

1. Clona este repositorio: `https://gitlab.com/cecilio1877/radio-humedales`
2. Instala las dependencias: `npm install` o `yarn install`

## Configuración

1. [Descarga e instala Expo Go](https://expo.dev/client) en tu dispositivo móvil.
2. Ejecuta el proyecto localmente: `npm expo start` o `yarn expo start`.
3. Escanea el código QR que aparece en la consola Expo con la aplicación Expo Go en tu dispositivo móvil.

## Uso

Cuando se prueba con Expo Go demora en reproducir el recurso pero es normal.

## Compilar

1. Crear una cuenta en Expo [cuenta en expo ](https://expo.dev/signup) 
2. Instalar EAS `npm install -g eas-cli` [más información](https://docs.expo.dev/build/setup/)
3. Logearse con la cuenta expo `eas login` desde el terminal donde esta el proyecto
4. `eas build:configure`
5. Se puede configurar el archivo eas.json con diferentes perfiles para generar una app para producción o pruebas [más información](https://docs.expo.dev/build/eas-json/)
6. Para android usar la configuración que está `eas build -p android --profile preview`

## Contribución

¡Contribuciones son bienvenidas! Si deseas contribuir a este proyecto, por favor sigue estos pasos:

en proceso ...


## Contacto

Para cualquier pregunta o sugerencia, no dudes en contactarme en [contacto@lalibre.net](mailto:contacto@lalibre.net).
